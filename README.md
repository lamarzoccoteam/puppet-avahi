# avahi

#### Table of Contents

1. [Description](#description)
2. [Setup](#setup)
3. [Usage](#usage)
4. [Reference](#reference)
5. [Limitations](#limitations)
6. [Development](#development)

## Description

The avahi module installs, configures, and manages the Avahi daemon.

While it could work on other platforms, it has been tested only on Linux
Ubuntu 14.04 LTS (Trusty).

## Setup

```
puppet module install emyl-avahi
```

The avahi module requires [puppetlabs-stdlib](https://forge.puppetlabs.com/puppetlabs/stdlib)
version 4.6.0 or newer. On Puppet Enterprise you must meet this requirement
before installing the module.

## Usage

To install the avahi daemon with sane defaults:

```puppet
include avahi
```

Nearly all configuration parameters are customizable:

```puppet
class { 'avahi':
  enable_reflector => true,
  use_ipv6         => false,
}
```

See reference for deeper details.

## Reference

### Classes

#### Public Classes

* `avahi`: Main class. Manage paramters for installing and configuring avahi.

#### Private Classes

* `avahi::config`: Manage the daemon configuration file.
* `avahi::install`: Manage the installation package.
* `avahi::service`: Manage the daemon service.

### Parameters

All parameters are optional.

####`add_service_cookie`
Add an implicit TXT entry to all locally registered services, containing a
cookie value which is chosen randomly on daemon startup.

####`allow_interfaces`
List of allowed network interfaces that should be used by the avahi-daemon.
Other interfaces will be ignored. By default all local interfaces except
loopback and point-to-point will be used.

####`allow_point_to_point`
Make use of interfaces with the POINTOPOINT flag set.

####`browse_domains`
Comma separated list of browsing domains (in addition to the default one and
those announced inside the default browsing domain).

####`cache_entries_max`
How many resource records are cached per interface.

####`check_response_ttl`
Ignore incoming IP packets unless the IP TTL is 255.

####`clients_max`
Maximum number of concurrent D-Bus clients allowed.

####`deny_interfaces`
List of network interfaces that should be ignored by avahi-daemon.
Other not specified interfaces will be used, unless `allow-interfaces` is set.

####`disable_publishing`
Start Avahi in a querying-only mode, publishing no records, not even address
records for the local host.

####`disable_user_service_publishing`
Don't allow other user applications to publish services.

####`disallow_other_stacks`
Don't allow other process to bind to UDP port 5353.

####`domain_name`
Default domain name avahi-daemon tries to register its host name and services
on the LAN in. Defaults to ".local".

####`enable_dbus`
If true, avahi-daemon connects to D-Bus, offering an object oriented client
API. If "warn", behaves like "yes" but the daemon starts up even when it fails
to connect to a D-Bus daemon.

####`enable_reflector`
Reflect incoming mDNS requests to all local network interfaces, effectively
allowing clients to browse mDNS/DNS-SD services on all networks connected to
the gateway.

####`enable_wide_area`
Enable DNS-SD over unicast DNS. If this is enabled only domains ending in
.local will be resolved on mDNS, all other domains are resolved via unicast
DNS.

####`entries_per_entry_group_max`
Maximum number of entries (resource records) per entry group registered by a
D-Bus client at a time.

####`host_name`
Host name avahi-daemon tries to register on the LAN. Defaults to the system
host name as set with the sethostname() system call.

####`objects_per_client_max`
Maximum number of object  (entry groups, browsers, resolvers) that may be
registered per D-Bus client at a time.

####`publish_aaaa_on_ipv4`
Publish an IPv6 AAAA record via IPv4, i.e. the local IPv6 addresses can be
resolved using an IPv4 transport.

####`publish_a_on_ipv6`
Publish an IPv4 A record via IPv6, i.e. the local IPv4 addresses can be
resolved using an IPv6 transport.

####`publish_addresses`
Register mDNS address records for all local IP addresses.

####`publish_dns_servers`
Comma separated list of IP addresses for unicast DNS servers. You can use this
to announce unicast DNS servers via mDNS.

####`publish_domain`
Announce the locally used domain name for browsing by other hosts.

####`publish_hinfo`
Register an mDNS HINFO record on all interfaces which contains information
about the local operating system and CPU.

####`publish_resolv_conf_dns_servers`
Publish the unicast DNS servers specified in /etc/resolv.conf, in addition to
those specified with `publish-dns-servers`.

####`publish_workstation`
Register a service of type "\_workstation.\_tcp" on the local LAN.

####`ratelimit_burst`
Per-interface packet rate-limiting burst parameter.

####`ratelimit_interval_usec`
Per-interface packet rate-limiting interval parameter.

####`reflect_ipv`
With reflector enabled, forward mDNS traffic between IPv4 and IPv6.

####`rlimit_core`
Value in bytes for RLIMIT_CORE.

####`rlimit_data`
Value in bytes for RLIMIT_DATA.

####`rlimit_fsize`
Value in bytes for RLIMIT_FSIZE.

####`rlimit_nofile`
Value in bytes for RLIMIT_NOFILE.

####`rlimit_nproc`
Value in bytes for RLIMIT_NPROC.

####`rlimit_stack`
Value in bytes for RLIMIT_STACK.

####`use_iff_running`
Monitors the IFF_RUNNING flag bit which is used by some (modern) network
drivers to tell user space if a network cable is plugged in (in case of copper
ethernet), or the network card is associated with some kind of network (in
case of WLAN).

####`use_ipv4`
Use IPv4 sockets.

####`use_ipv6`
Use IPv6 sockets.

## Limitations

Only tested on Linux Ubuntu 14.04 LTS (Trusty) / Puppet Enterprise 2015.3

## Development

Contributions are welcome.
