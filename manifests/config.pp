# Private class
class avahi::config {

  file { '/etc/avahi/avahi-daemon.conf':
    ensure  => present,
    content => template('avahi/avahi-daemon.conf.erb'),
  }

}
