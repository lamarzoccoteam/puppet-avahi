# Private class
class avahi::install {

  package { 'avahi-daemon':
    ensure => present,
  }

}
