# Class: avahi
# ===========================
#
# Installs, configures, and manages the Avahi daemon.
#
# Parameters
# ----------
#
# * `add_service_cookie`
# Add an implicit TXT entry to all locally registered services, containing a
# cookie value which is chosen randomly on daemon startup.
#
# * `allow_interfaces`
# List of allowed network interfaces that should be used by the avahi-daemon.
# Other interfaces will be ignored. By default all local interfaces except
# loopback and point-to-point will be used.
#
# * `allow_point_to_point`
# Make use of interfaces with the POINTOPOINT flag set.
#
# * `browse_domains`
# Comma separated list of browsing domains (in addition to the default one and
# those announced inside the default browsing domain).
#
# * `cache_entries_max`
# How many resource records are cached per interface.
#
# * `check_response_ttl`
# Ignore incoming IP packets unless the IP TTL is 255.
#
# * `clients_max`
# Maximum number of concurrent D-Bus clients allowed.
#
# * `deny_interfaces`
# List of network interfaces that should be ignored by avahi-daemon.
# Other not specified interfaces will be used, unless `allow-interfaces` is set.
#
# * `disable_publishing`
# Start Avahi in a querying-only mode, publishing no records, not even address
# records for the local host.
#
# * `disable_user_service_publishing`
# Don't allow other user applications to publish services.
#
# * `disallow_other_stacks`
# Don't allow other process to bind to UDP port 5353.
#
# * `domain_name`
# Default domain name avahi-daemon tries to register its host name and services
# on the LAN in. Defaults to ".local".
#
# * `enable_dbus`
# If true, avahi-daemon connects to D-Bus, offering an object oriented client
# API. If "warn", behaves like "yes" but the daemon starts up even when it fails
# to connect to a D-Bus daemon.
#
# * `enable_reflector`
# Reflect incoming mDNS requests to all local network interfaces, effectively
# allowing clients to browse mDNS/DNS-SD services on all networks connected to
# the gateway.
#
# * `enable_wide_area`
# Enable DNS-SD over unicast DNS. If this is enabled only domains ending in
# .local will be resolved on mDNS, all other domains are resolved via unicast
# DNS.
#
# * `entries_per_entry_group_max`
# Maximum number of entries (resource records) per entry group registered by a
# D-Bus client at a time.
#
# * `host_name`
# Host name avahi-daemon tries to register on the LAN. Defaults to the system
# host name as set with the sethostname() system call.
#
# * `objects_per_client_max`
# Maximum number of object  (entry groups, browsers, resolvers) that may be
# registered per D-Bus client at a time.
#
# * `publish_aaaa_on_ipv4`
# Publish an IPv6 AAAA record via IPv4, i.e. the local IPv6 addresses can be
# resolved using an IPv4 transport.
#
# * `publish_a_on_ipv6`
# Publish an IPv4 A record via IPv6, i.e. the local IPv4 addresses can be
# resolved using an IPv6 transport.
#
# * `publish_addresses`
# Register mDNS address records for all local IP addresses.
#
# * `publish_dns_servers`
# Comma separated list of IP addresses for unicast DNS servers. You can use this
# to announce unicast DNS servers via mDNS.
#
# * `publish_domain`
# Announce the locally used domain name for browsing by other hosts.
#
# * `publish_hinfo`
# Register an mDNS HINFO record on all interfaces which contains information
# about the local operating system and CPU.
#
# * `publish_resolv_conf_dns_servers`
# Publish the unicast DNS servers specified in /etc/resolv.conf, in addition to
# those specified with `publish-dns-servers`.
#
# * `publish_workstation`
# Register a service of type "_workstation._tcp" on the local LAN.
#
# * `ratelimit_burst`
# Per-interface packet rate-limiting burst parameter.
#
# * `ratelimit_interval_usec`
# Per-interface packet rate-limiting interval parameter.
#
# * `reflect_ipv`
# With reflector enabled, forward mDNS traffic between IPv4 and IPv6.
#
# * `rlimit_core`
# Value in bytes for RLIMIT_CORE.
#
# * `rlimit_data`
# Value in bytes for RLIMIT_DATA.
#
# * `rlimit_fsize`
# Value in bytes for RLIMIT_FSIZE.
#
# * `rlimit_nofile`
# Value in bytes for RLIMIT_NOFILE.
#
# * `rlimit_nproc`
# Value in bytes for RLIMIT_NPROC.
#
# * `rlimit_stack`
# Value in bytes for RLIMIT_STACK.
#
# * `use_iff_running`
# Monitors the IFF_RUNNING flag bit which is used by some (modern) network
# drivers to tell user space if a network cable is plugged in (in case of copper
# ethernet), or the network card is associated with some kind of network (in
# case of WLAN).
#
# * `use_ipv4`
# Use IPv4 sockets.
#
# * `use_ipv6`
# Use IPv6 sockets.
#
# Examples
# --------
#
# @example
#    class { 'avahi':
#      enable_reflector => true,
#    }
#
# Authors
# -------
#
# Emiliano Ticci <emiliano@lamarzocco.com>
#
# Copyright
# ---------
#
# Copyright 2016 La Marzocco Srl.
#
class avahi(
  $allow_interfaces                = undef,
  $add_service_cookie              = false,
  $allow_point_to_point            = false,
  $browse_domains                  = undef,
  $cache_entries_max               = 4096,
  $check_response_ttl              = false,
  $clients_max                     = 4096,
  $deny_interfaces                 = undef,
  $disable_publishing              = false,
  $disable_user_service_publishing = false,
  $disallow_other_stacks           = false,
  $domain_name                     = undef,
  $enable_dbus                     = true,
  $enable_reflector                = false,
  $enable_wide_area                = true,
  $entries_per_entry_group_max     = 32,
  $host_name                       = undef,
  $objects_per_client_max          = 1024,
  $publish_aaaa_on_ipv4            = true,
  $publish_a_on_ipv6               = false,
  $publish_addresses               = true,
  $publish_dns_servers             = undef,
  $publish_domain                  = true,
  $publish_hinfo                   = true,
  $publish_resolv_conf_dns_servers = false,
  $publish_workstation             = true,
  $ratelimit_burst                 = 1000,
  $ratelimit_interval_usec         = 1000000,
  $reflect_ipv                     = false,
  $rlimit_core                     = 0,
  $rlimit_data                     = 4194304,
  $rlimit_fsize                    = 0,
  $rlimit_nofile                   = 768,
  $rlimit_nproc                    = 3,
  $rlimit_stack                    = 4194304,
  $use_iff_running                 = false,
  $use_ipv4                        = true,
  $use_ipv6                        = true,
) {

  validate_string($allow_interfaces)
  validate_bool($add_service_cookie)
  validate_bool($allow_point_to_point)
  validate_string($browse_domains)
  validate_integer($cache_entries_max, undef, 0)
  validate_bool($check_response_ttl)
  validate_integer($clients_max, undef, 0)
  validate_string($deny_interfaces)
  validate_bool($disable_publishing)
  validate_bool($disable_user_service_publishing)
  validate_bool($disallow_other_stacks)
  validate_string($domain_name)
  unless is_bool($enable_dbus) or $enable_dbus == 'warn' {
    fail('"enable_dbus" is not a boolean and is not "warn".')
  }
  validate_bool($enable_reflector)
  validate_bool($enable_wide_area)
  validate_integer($entries_per_entry_group_max, undef, 0)
  validate_string($host_name)
  validate_integer($objects_per_client_max, undef, 0)
  validate_bool($publish_aaaa_on_ipv4)
  validate_bool($publish_a_on_ipv6)
  validate_bool($publish_addresses)
  validate_string($publish_dns_servers)
  validate_bool($publish_domain)
  validate_bool($publish_hinfo)
  validate_bool($publish_resolv_conf_dns_servers)
  validate_bool($publish_workstation)
  validate_integer($ratelimit_burst, undef, 0)
  validate_integer($ratelimit_interval_usec, undef, 0)
  validate_bool($reflect_ipv)
  validate_integer($rlimit_core, undef, 0)
  validate_integer($rlimit_data, undef, 0)
  validate_integer($rlimit_fsize, undef, 0)
  validate_integer($rlimit_nofile, undef, 0)
  validate_integer($rlimit_nproc, undef, 0)
  validate_integer($rlimit_stack, undef, 0)
  validate_bool($use_iff_running)
  validate_bool($use_ipv4)
  validate_bool($use_ipv6)

  anchor { 'avahi::begin': }     ->
  class  { '::avahi::install': } ->
  class  { '::avahi::config': }  ~>
  class  { '::avahi::service': } ->
  anchor { 'avahi::end': }

}
