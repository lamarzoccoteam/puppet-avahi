# Private class
class avahi::service {

  service { 'avahi-daemon':
    ensure     => 'running',
    enable     => true,
    hasrestart => true,
  }

}
