require 'spec_helper'

describe 'avahi' do

  it { should contain_class('avahi') }
  it { should contain_class('avahi::install') }
  it { should contain_class('avahi::config') }
  it { should contain_class('avahi::service') }

  context 'avahi::install' do
    it { should contain_package('avahi-daemon') }
  end

  context 'avahi::service' do
    it do
      is_expected.to contain_service('avahi-daemon').with({
        'ensure'     => 'running',
        'enable'     => 'true',
        'hasrestart' => 'true',
      })
    end
  end

  context 'with boolean parameters' do
    [
      :add_service_cookie,
      :allow_point_to_point,
      :check_response_ttl,
      :disable_publishing,
      :disable_user_service_publishing,
      :disallow_other_stacks,
      :enable_dbus,
      :enable_reflector,
      :enable_wide_area,
      :publish_aaaa_on_ipv4,
      :publish_a_on_ipv6,
      :publish_addresses,
      :publish_domain,
      :publish_hinfo,
      :publish_resolv_conf_dns_servers,
      :publish_workstation,
      :reflect_ipv,
      :use_iff_running,
      :use_ipv4,
      :use_ipv6
    ].each do |parameter|
      context "with #{parameter} => true" do
        let(:params) { { parameter => true } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .with_content(/^#{raise_dash(parameter)}=yes$/)
        end
      end

      context "with #{parameter} => false" do
        let(:params) { { parameter => false } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .with_content(/^#{raise_dash(parameter)}=no$/)
        end
      end

      context "with #{parameter} not valid" do
        let(:params) { { parameter => 'foo' } }
        it do
          is_expected.to compile.and_raise_error(/is not a boolean/)
        end
      end
    end
  end

  context 'with enable_dbus => "warn"' do
    let(:params) { { :enable_dbus => 'warn' } }

    it do
      is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
        .with_content(/^enable-dbus=warn$/)
    end
  end

  context 'with integer parameters' do
    [
      :cache_entries_max,
      :clients_max,
      :entries_per_entry_group_max,
      :objects_per_client_max,
      :ratelimit_burst,
      :ratelimit_interval_usec,
      :rlimit_core,
      :rlimit_data,
      :rlimit_fsize,
      :rlimit_nofile,
      :rlimit_nproc,
      :rlimit_stack
    ].each do |parameter|
      context "with #{parameter} == integer" do
        let(:params) { { parameter => 123 } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .with_content(/^#{raise_dash(parameter)}=123$/)
        end
      end

      context "with #{parameter} == 0" do
        let(:params) { { parameter => 0 } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .with_content(/^#{raise_dash(parameter)}=0$/)
        end
      end

      context "with #{parameter} == negative integer" do
        let(:params) { { parameter => -123 } }
        it do
          is_expected.to compile.and_raise_error(/greater or equal to 0/)
        end
      end

      context "with #{parameter} == string" do
        let(:params) { { parameter => 'foo' } }
        it do
          is_expected.to compile.and_raise_error(/to be an Integer/)
        end
      end
    end
  end

  context 'with string parameters' do
    [
      :allow_interfaces,
      :browse_domains,
      :deny_interfaces,
      :domain_name,
      :host_name,
      :publish_dns_servers
    ].each do |parameter|
      context "with #{parameter} == string" do
        let(:params) { { parameter => 'foo' } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .with_content(/^#{raise_dash(parameter)}=foo$/)
        end
      end

      context "with #{parameter} == undef" do
        let(:params) { { parameter => :undef } }
        it do
          is_expected.to contain_file('/etc/avahi/avahi-daemon.conf') \
            .without_content(/^#{raise_dash(parameter)}$/)
        end
      end

      context "with #{parameter} != string" do
        let(:params) { { parameter => 123 } }
        it do
          is_expected.to compile.and_raise_error(/is not a string/)
        end
      end
    end
  end

end
