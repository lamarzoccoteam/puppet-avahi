require 'puppetlabs_spec_helper/module_spec_helper'

def raise_dash(str)
  str.to_s.gsub('_', '-')
end
